/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [Alicia Grillo Gonzalez]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    
    //**************************************************************************************************************************************
            //FADE IN - PANTALLA DE LOGO
    
    bool FadeIn = true;
    float alpha = 0.0f;
    
    //**************************************************************************************************************************************
            //PANTALLA DE TITULO
            
    bool blink = false;
    
    Vector2 titlePos = {screenWidth/2, -100};
    
    //**************************************************************************************************************************************
            //GAMEPLAY
    
    ///////////////////////////////////velocidad de la pelota
    const int ballSize = 25;
    
    const int velocity = 5;
    const int maxVelocity = 12;
    const int minVelocity = 4;
    
    /////////////////////////////////el pausa
    bool pause = false;
    
    ////////////////////////////////////las palas
    Rectangle paladerecha;
   
    paladerecha.width = 20;
    paladerecha.height = 100;    
    paladerecha.x = screenWidth - 50 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
   
   
    Rectangle palaizquierda;
   
    palaizquierda.width = 20;
    palaizquierda.height = 100;    
    palaizquierda.x = 50;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
    
    /////////////////////////////////////////la pelota
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
   
    Vector2 ballVelocity;
    ballVelocity.x = minVelocity;
    ballVelocity.y = minVelocity;
    
    ///////////////////////////////////////////GOLES
    int playerGoal = 0;             //Resto la vida mediante los goles
    int enemyGoal = 0;    
    
    ////////////////////////////////////////////BARRAS DE VIDA
    Rectangle LifePlayerBack;
   
    LifePlayerBack.width = 240;
    LifePlayerBack.height = 30;
    LifePlayerBack.x = 100;
    LifePlayerBack.y = screenHeight/2 - 200 - LifePlayerBack.height/2;
   
    Rectangle LifePlayer;
   
    LifePlayer.width = 240;
    LifePlayer.height = 30;
    LifePlayer.x = 100;
    LifePlayer.y = screenHeight/2 - 200 - LifePlayerBack.height/2;
   
    Rectangle LifeEnemyBack;
   
    LifeEnemyBack.width = 240;
    LifeEnemyBack.height = 30;
    LifeEnemyBack.x = screenWidth - 75 - LifeEnemyBack.width;
    LifeEnemyBack.y = screenHeight/2 - 200 - LifeEnemyBack.height/2;
   
    Rectangle LifeEnemy;
   
    LifeEnemy.width = 240;
    LifeEnemy.height = 30;
    LifeEnemy.x = screenWidth - 75 - LifeEnemyBack.width;
    LifeEnemy.y = screenHeight/2 - 200 - LifeEnemyBack.height/2;
    
    ///////////////////IA EN EL MEDIO DE LA PANTALLA
    
    int iaLinex = screenWidth/2;
    
    //**************************************************************************************************************************************
            //ENING

    Texture2D Victoria; //= LoadTexture("Resources/victory.png");
    //PantallaGameplay.height = 450;
    //PantallaGameplay.width = 800;
   
    Texture2D Derrota; //= LoadTexture("Resources/loose.png");
    //PantallaGameplay.height = 450;
    //PantallaGameplay.width = 800;
    
    
    // NOTE: Here there are some useful variables (should be initialized)
    
    Rectangle player;
    int playerSpeedY;
    
    Rectangle enemy;
    int enemySpeedY;
    
    Vector2 ballPosition;
    Vector2 ballSpeed;
    int ballRadius;
    
    int playerLife;
    int enemyLife;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
            
            if(FadeIn)
                {
                    alpha += 1.0f/90;
                    
                    if(alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                        
                        framesCounter++;
                        if(framesCounter % 120 == 0)
                        {
                            framesCounter = 0;
                            FadeIn = false;
                        }
                    }
                }
                else
                {
                    alpha -= 1.0f/90;
                    
                    if(alpha <= 0.0f)
                    {
                        framesCounter = 0;
                        screen = TITLE;
                    }
                }
                
                if(IsKeyPressed(KEY_ENTER))
                {
                    framesCounter = 0;
                    screen = TITLE;
                }
                
            } break;
            
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                
                if(titlePos.y < 100)
                {
                    titlePos.y += 2;
                    
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        titlePos.y =100;
                    }
                }
                else
                {
                    titlePos.y = 100;
               
                // TODO: "PRESS ENTER" logic.........................(0.5p)
               
                    framesCounter++;
                    if (framesCounter % 30 == 0)
                        {
                            framesCounter = 0;
                            blink = !blink;
                        }
                       
                        if (IsKeyPressed (KEY_ENTER))
                        {
                            screen = GAMEPLAY;
                            framesCounter = 0;
                        }
                        
                }
                
            } break;
            
            case GAMEPLAY:
            { 
            ///////////////////////////////// TODO: Player movement logic.......................(0.2p)
            
            /////MOVIMIENTO DEL JUGADOR  
            if(!pause)
            {
                if (IsKeyDown(KEY_UP))
                {
                  palaizquierda.y -= velocity;
                }
               
                if (IsKeyDown(KEY_DOWN))
                {
                  palaizquierda.y += velocity;
                }
            
            
                ////////////////////////////////// TODO: Enemy movement logic (IA)...................(1p)
                
                //////MOVIMIENTO DEL IA ENEMIGO
                
                if (ball.x > iaLinex)
                    {
                        if (ball.y > paladerecha.y){
                            paladerecha.y += velocity *0.9;
                        }
                       
                        if (ball.y < paladerecha.y){
                            paladerecha.y -= velocity *0.9;
                        }
                    }
                    
                ////////////////////////////////////////Limites de las palas
                if(palaizquierda.y<0)
                {
                    palaizquierda.y = 0;
                }
               
                if(palaizquierda.y > (screenHeight - palaizquierda.height))
                {
                    palaizquierda.y = screenHeight - palaizquierda.height;
                }
               
                if(paladerecha.y<0)
                {
                    paladerecha.y = 0;
                }
               
                if(paladerecha.y > (screenHeight - paladerecha.height))
                {
                    paladerecha.y = screenHeight - paladerecha.height;
                }
               
               ///////////////////////////////////////// TODO: Ball movement logic.........................(0.2p)
               
                //Gestionamos el movimiento de la Bola
                
                    ball.x += ballVelocity.x;
                    ball.y += ballVelocity.y;
                
                /////////////////////////////////////////////// TODO: Collision detection (ball-limits) logic.....(1p)
                ////////////////////////////////////////////// TODO: Life bars decrease logic....................(1p)
                
                //LIMITE DE LA PELOTA Y LA BARRAS DE VIDA
                if (ball.x > screenWidth - ballSize)
                    {
                    
                        ball.x = screenWidth/2;
                        ball.y = screenHeight/2;
                        ballVelocity.x = minVelocity;
                        ballVelocity.y = minVelocity;
                        playerGoal = playerGoal + 1;
                        LifeEnemy.width = LifeEnemy.width - 25;
                       
                    }
                    else if (ball.x < ballSize)
                    {
              
                        ball.x = screenWidth/2;
                        ball.y = screenHeight/2;
                        ballVelocity.x = minVelocity;
                        ballVelocity.y = minVelocity;
                        enemyGoal = enemyGoal + 1;
                        LifePlayer.width = LifePlayer.width - 25;
                    }
                   
                    if ((ball.y > screenHeight - ballSize) || (ball.y < ballSize))
                    {
                      
                        ballVelocity.y *=-1;
                    }
                
                //QUE LA BOLA CHOQUE CON LAS PALAS
                
                /////////////////////////////////////////// TODO: Collision detection (ball-player) logic.....(0.5p)
                
                if (CheckCollisionCircleRec (ball, ballSize, palaizquierda)){
                    if (ballVelocity.x<0){
                        if (abs (ballVelocity.x) < maxVelocity){
                            ballVelocity.x *=-1;
                            ballVelocity.y *=1;
                        }
                        else
                        {
                            ballVelocity.x *=1;
                        }
                    }
                }
                
                //////////////////////////////////////////// TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                if (CheckCollisionCircleRec (ball, ballSize, paladerecha)){
                    if (ballVelocity.x>0){
                        if (abs (ballVelocity.x) < maxVelocity){
                            ballVelocity.x *=-1;
                            ballVelocity.y *=1;
                        }
                        else
                        {
                            ballVelocity.x *=1;
                        }
                    }
                }
                
                ///////////////////////////////////////////// TODO: Time counter logic..........................(0.2p)
                
                //CONTRARELOJ
                framesCounter++;
                {
                    if (framesCounter > 60)
                        {
                            secondsCounter--;
                            framesCounter = 0;
                        }
                       
                        if (secondsCounter == 0)
                        {
                            screen = ENDING;
                        }
                }        
                //////////////////////////////////////////// TODO: Game ending logic...........................(0.2p)
                
                //PLAYER GANA, ENEMY PIERDE
                if (playerGoal == 10)
                {
                    screen = ENDING;
                }
               
                //ENEMY GANA, PLAYER PIERDE
                if (enemyGoal == 10)
                {
                    screen = ENDING;
                }
            }

                            
                ////////////////////////////////////////////// TODO: Pause button logic..........................(0.2p)
                
                ///////PAUSA
                    if (IsKeyPressed (KEY_P))
                    {
                        pause = !pause;
                    }
                   
                    if (!pause)
                    {
                        ball.x += ballVelocity.x;
                        ball.y += ballVelocity.y;
                    }
                    
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
               
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyPressed (KEY_SPACE))
                {
                    CloseWindow();
                }
               
                if (IsKeyPressed (KEY_ENTER))
                {
                    screen = GAMEPLAY;
                }

            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                    DrawText ("Alicia", screenWidth/2 - MeasureText ("Alicia", 50)/2, screenHeight/2 - 50, 50, Fade (PINK, alpha));
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    DrawRectangle(0,0, screenWidth, screenHeight, BLACK);
                    
                    // TODO: Draw Title..............................(0.2p)
                    
                    DrawText("RecuPONG", 85, 100, 120, PINK);
                   
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (blink) DrawText("Press ENTER to start", screenWidth/2 - MeasureText ("Press ENTER to start", 30)/2, screenHeight/2,30, GREEN);
                    
                } break;
                case GAMEPLAY:
                { 
                    // Pinto Pala Derecha
                    DrawRectangleRec(paladerecha, GREEN);
                   
                    // Pinto Pala Izquierda
                    DrawRectangleRec(palaizquierda, PINK);
                   
                    // Pintamos la pelota
                    DrawCircleV(ball, ballSize, YELLOW);
                    
                    // Pintamos la pantalla de pausa
                    if (pause)
                    {
                        DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });
                        DrawText ("Press p to continue", screenWidth/2 - MeasureText ("Press p to continue", 40)/2, screenHeight/2, 40, DARKBLUE);
                    }
                    
                    //Barras de vida del jugador
                    DrawRectangleRec (LifePlayerBack, DARKGRAY);
                   
                    DrawRectangleRec (LifePlayer, DARKGREEN);
                   
                    //Barras de vida del enemigo
                    DrawRectangleRec (LifeEnemyBack, DARKGRAY);
                   
                    DrawRectangleRec (LifeEnemy, PURPLE);
                    
                    //contrareloj
                    
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2 - 20, 10 , 40, DARKGRAY);
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    ClearBackground (WHITE);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                   
                    if (playerGoal == 10)
                    {
                        DrawTexture(Victoria, 0, 0, PURPLE);
                       
                        DrawText ("You WON:D", screenWidth/2 - MeasureText ("You WON:D", 40)/2, screenHeight/2 - 40, 40, DARKGREEN);
                    }
                    else if (enemyGoal == 10)
                    {
                        DrawTexture(Derrota, 0, 0, GREEN);
                        DrawText ("You LOOSE D:", screenWidth/2 - MeasureText ("You LOOSE D:", 40)/2, screenHeight/2 - 40, 40, DARKPURPLE);
                    }
                    else
                    {
                        DrawText ("Time's UP!", screenWidth/2 - MeasureText ("Time's UP!", 40)/2, screenHeight/2 - 40, 40, BLACK);
                    }
                   
                    DrawText ("Space - BYE BYE", screenWidth/4 - MeasureText ("Space - BYE BYE", 20)/2, screenHeight/2 + 30, 20, PINK);
                    DrawText ("Enter - RETRAY", 3*screenWidth/4 - MeasureText ("Enter - RETRAY", 20)/2, screenHeight/2 + 30, 20, PINK);
                   
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}